#ifndef OTHERLOOKANDFEEL_H_INCLUDED
#define OTHERLOOKANDFEEL_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#define KNOBC 15, 58, 80
#define M_PI 3.14159265358979323846

//==============================================================================

class OtherLookAndFeel : public LookAndFeel_V3
{
public:
	OtherLookAndFeel()
	{
		setColour(Slider::rotarySliderFillColourId, Colours::red);
	}

	void drawRotarySlider(Graphics& g, int x, int y, int width, int height, float sliderPos,
		const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider) override
	{
		//...
		const float radius = jmin(width / 2, height / 2) - 4.0f;
		const float centreX = x + width * 0.5f;
		const float centreY = y + height * 0.5f;
		const float rx = centreX - radius;
		const float ry = centreY - radius;
		const float rw = radius * 2.0f;
		const float angle = rotaryStartAngle + sliderPos * (rotaryEndAngle - rotaryStartAngle);

		// fill
		g.setColour(Colour(KNOBC));
		g.fillEllipse(rx, ry, rw, rw);
		// outline
		g.setColour(Colours::grey);
		g.drawEllipse(rx, ry, rw, rw, 3.0f);

		Path p;
		const float pointerLength = radius * 0.66f;
		const float pointerThickness = 4.0f;
		p.addRectangle(-pointerThickness * 0.5f, -radius, pointerThickness, pointerLength);
		p.applyTransform(AffineTransform::rotation(angle).translated(centreX, centreY));

		g.setColour(Colours::white);
		g.fillPath(p);
	}

	void drawToggleButton(Graphics& g, ToggleButton& button,
		bool isMouseOverButton, bool isButtonDown)
	{
		float fontSize = jmin(15.0f, button.getHeight() * 0.75f);
		const float tickWidth = fontSize * 1.1f;

		drawTickBox(g, button, 4.0f, (button.getHeight() - tickWidth) * 0.5f,
			tickWidth, tickWidth,
			button.getToggleState(),
			button.isEnabled(),
			isMouseOverButton,
			isButtonDown);

		g.setColour(button.findColour(ToggleButton::textColourId));
		g.setFont(fontSize);

		if (!button.isEnabled())
			g.setOpacity(0.5f);

		const int textX = (int)tickWidth + 5;

		g.drawFittedText(button.getButtonText(),
			textX, 0,
			button.getWidth() - textX - 2, button.getHeight(),
			Justification::centredLeft, 10);
	}

};

class OtherLookAndFeel2 : public LookAndFeel_V3
{
public:
	OtherLookAndFeel2()
	{
		setColour(Slider::rotarySliderFillColourId, Colours::red);
	}

	void drawRotarySlider(Graphics& g, int x, int y, int width, int height, float sliderPos,
		const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider) override
	{
		//...
		const float radius = jmin(width / 2, height / 2) - 4.0f;
		const float centreX = x + width * 0.5f;
		const float centreY = y + height * 0.5f;
		const float rx = centreX - radius;
		const float ry = centreY - radius;
		const float rw = radius * 2.0f;
		const float angle = rotaryStartAngle + sliderPos * (rotaryEndAngle - rotaryStartAngle);

		// fill
		//Colour()
		g.setColour(Colour(KNOBC));
		g.fillEllipse(rx, ry, rw, rw);
		// outline
		g.setColour(Colours::silver);
		g.drawEllipse(rx, ry, rw, rw, 3.0f);
		g.setColour(Colours::gold);

		const float innerRadius = radius * 0.2f;
		const float thickness = 0.7f;
		Path p;
		p.addTriangle(-innerRadius, 0.0f,
			0.0f, -radius * thickness * 1.1f,
			innerRadius, 0.0f);

		p.addEllipse(-innerRadius, -innerRadius, innerRadius * 2.0f, innerRadius * 2.0f);

		g.fillPath(p, AffineTransform::rotation(angle).translated(centreX, centreY));

	}
};


class OtherLookAndFeel3 : public LookAndFeel_V3
{
public:
	OtherLookAndFeel3()
	{
		setColour(Slider::rotarySliderFillColourId, Colours::red);
	}

	void drawRotarySlider(Graphics& g, int x, int y, int width, int height, float sliderPos,
		const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider)
	{
		const float radius = jmin(width / 2, height / 2) - 2.0f;
		const float centreX = x + width * 0.5f;
		const float centreY = y + height * 0.5f;
		const float rx = centreX - radius;
		const float ry = centreY - radius;
		const float rw = radius * 2.0f;
		const float angle = rotaryStartAngle + sliderPos * (rotaryEndAngle - rotaryStartAngle);
		const bool isMouseOver = slider.isMouseOverOrDragging() && slider.isEnabled();
		const float halfWidth = M_PI - 0.55f;

		if (radius > 12.0f)
		{
			if (slider.isEnabled())
				g.setColour(slider.findColour(Slider::rotarySliderFillColourId).withAlpha(isMouseOver ? 1.0f : 0.7f));
			else
				g.setColour(Colour(0x80808080));

			const float thickness = 0.7f;

			{
				Path filledArc;
				filledArc.addPieSegment(rx, ry, rw, rw, 0, halfWidth * sliderPos, thickness);
				filledArc.addPieSegment(rx, ry, rw, rw, 0, -1.0f * sliderPos * halfWidth, thickness);
				//filledArc.addPieSegment(rx, ry, rw, rw, 0, -angle, thickness);
				g.fillPath(filledArc);
			}

			{
				const float innerRadius = radius * 0.2f;
				Path p;
				p.addTriangle(-innerRadius, 0.0f,
					0.0f, -radius * thickness * 1.1f,
					innerRadius, 0.0f);

				p.addEllipse(-innerRadius, -innerRadius, innerRadius * 2.0f, innerRadius * 2.0f);

				g.fillPath(p, AffineTransform::rotation(angle).translated(centreX, centreY));
			}

			if (slider.isEnabled())
				g.setColour(slider.findColour(Slider::rotarySliderOutlineColourId));
			else
				g.setColour(Colour(0x80808080));

			Path outlineArc;
			outlineArc.addPieSegment(rx, ry, rw, rw, rotaryStartAngle, rotaryEndAngle, thickness);
			outlineArc.closeSubPath();

			g.strokePath(outlineArc, PathStrokeType(slider.isEnabled() ? (isMouseOver ? 2.0f : 1.2f) : 0.3f));
		}
		else
		{
			if (slider.isEnabled())
				g.setColour(slider.findColour(Slider::rotarySliderFillColourId).withAlpha(isMouseOver ? 1.0f : 0.7f));
			else
				g.setColour(Colour(0x80808080));

			Path p;
			p.addEllipse(-0.4f * rw, -0.4f * rw, rw * 0.8f, rw * 0.8f);
			PathStrokeType(rw * 0.1f).createStrokedPath(p, p);

			p.addLineSegment(Line<float>(0.0f, 0.0f, 0.0f, -radius), rw * 0.2f);

			g.fillPath(p, AffineTransform::rotation(angle).translated(centreX, centreY));
		}
	}
};

class OtherLookAndFeel4 : public LookAndFeel_V3
{
	void drawRotarySlider(Graphics& g, int x, int y, int width, int height, float sliderPos,
		const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider)
	{
		const float radius = jmin(width / 2, height / 2) - 2.0f;
		const float centreX = x + width * 0.5f;
		const float centreY = y + height * 0.5f;
		const float rx = centreX - radius;
		const float ry = centreY - radius;
		const float rw = radius * 2.0f;
		const float angle = rotaryStartAngle + sliderPos * (rotaryEndAngle - rotaryStartAngle);
		const bool isMouseOver = slider.isMouseOverOrDragging() && slider.isEnabled();

		if (radius > 12.0f)
		{
			if (slider.isEnabled())
				g.setColour(slider.findColour(Slider::rotarySliderFillColourId).withAlpha(isMouseOver ? 1.0f : 0.7f));
			else
				g.setColour(Colour(0x80808080));

			const float thickness = 0.7f;

			{
				Path filledArc;
				filledArc.addPieSegment(rx, ry, rw, rw, rotaryEndAngle, angle, thickness);
				g.fillPath(filledArc);
			}

			{
				const float innerRadius = radius * 0.2f;
				Path p;
				p.addTriangle(-innerRadius, 0.0f,
					0.0f, -radius * thickness * 1.1f,
					innerRadius, 0.0f);

				p.addEllipse(-innerRadius, -innerRadius, innerRadius * 2.0f, innerRadius * 2.0f);

				g.fillPath(p, AffineTransform::rotation(angle).translated(centreX, centreY));
			}

			if (slider.isEnabled())
				g.setColour(slider.findColour(Slider::rotarySliderOutlineColourId));
			else
				g.setColour(Colour(0x80808080));

			Path outlineArc;
			outlineArc.addPieSegment(rx, ry, rw, rw, rotaryStartAngle, rotaryEndAngle, thickness);
			outlineArc.closeSubPath();

			g.strokePath(outlineArc, PathStrokeType(slider.isEnabled() ? (isMouseOver ? 2.0f : 1.2f) : 0.3f));
		}
		else
		{
			if (slider.isEnabled())
				g.setColour(slider.findColour(Slider::rotarySliderFillColourId).withAlpha(isMouseOver ? 1.0f : 0.7f));
			else
				g.setColour(Colour(0x80808080));

			Path p;
			p.addEllipse(-0.4f * rw, -0.4f * rw, rw * 0.8f, rw * 0.8f);
			PathStrokeType(rw * 0.1f).createStrokedPath(p, p);

			p.addLineSegment(Line<float>(0.0f, 0.0f, 0.0f, -radius), rw * 0.2f);

			g.fillPath(p, AffineTransform::rotation(angle).translated(centreX, centreY));
		}
	}
};

class MyLevelMeterLookAndFeel : public LookAndFeel_V3, FFAU::LevelMeter::LookAndFeelMethods
{
public:
	juce::Rectangle<float> getMeterMaxNumberBounds(const juce::Rectangle<float> bounds,
		const FFAU::LevelMeter::MeterFlags meterType) const override
	{
		if (meterType & FFAU::LevelMeter::Minimal) {
			return juce::Rectangle<float>();
		}
		else if (meterType & FFAU::LevelMeter::Vintage) {
			return bounds;
		}
		else {
			if (meterType & FFAU::LevelMeter::Horizontal) {
				const float margin = bounds.getHeight() * 0.05;
				return juce::Rectangle<float>(bounds.getX() + margin,
					bounds.getCentreY() + margin,
					60,
					bounds.getHeight() * 0.5 - margin * 2.0);
			}
			else {
				const float margin = bounds.getWidth() * 0.05;
				return juce::Rectangle<float>(bounds.getX() + margin,
					bounds.getBottom() - (margin + 25),
					bounds.getWidth() - 2 * margin,
					25.0);
			}
		}
	}
	
};


#endif  // OTHERLOOKANDFEEL_H_INCLUDED
