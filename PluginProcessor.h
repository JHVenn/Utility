#ifndef PLUGINPROCESSOR_H_INCLUDED
#define PLUGINPROCESSOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include <ATK/Core/InPointerFilter.h>
#include <ATK/EQ/ButterworthFilter.h>
#include <ATK/EQ/IIRFilter.h>
#include <ATK/Core/OutPointerFilter.h>
#include <ATK/Core/PipelineGlobalSinkFilter.h>

#define HPFORDER 2

//==============================================================================
/**
*/

class ValueSmoother
{
public:
	ValueSmoother();
	~ValueSmoother();
	void reset(double sampleRate, int samplesPerBlock, double initValue, double time);
	void setTarget(double target);
	float getNextValue();
	float getTarget();

private:
	float m_currentValue;
	float m_target;
	float m_interval;
	double m_sampleRate;
	double m_time;
	int m_samplesPerBlock;
};

class UtilityAudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    UtilityAudioProcessor();
    ~UtilityAudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (AudioSampleBuffer&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

	AudioParameterFloat *p_panL;
	AudioParameterFloat *p_panR;
	AudioParameterFloat *p_prePan;

	AudioParameterInt *p_input;
	AudioParameterBool *p_mute;
	AudioParameterBool *p_pad;
	AudioParameterBool *p_DCoffset;

	AudioParameterFloat *p_gain;
	AudioParameterFloat *p_pan;

	AudioParameterFloat *p_gainL;
	AudioParameterFloat *p_gainR;
	AudioParameterBool *p_phaseL;
	AudioParameterBool *p_phaseR;
	AudioParameterBool *p_muteL;
	AudioParameterBool *p_muteR;
	
	AudioParameterFloat *p_gainM;
	AudioParameterFloat *p_gainS;
	AudioParameterFloat *p_width;
	AudioParameterBool *p_muteM;
	AudioParameterBool *p_muteS;

	AudioParameterFloat *p_lowPass;
	AudioParameterFloat *p_highPass;

	AudioParameterBool *p_hpfOn;
	AudioParameterBool *p_lpfOn;

	AudioParameterBool *p_hardClip;
	AudioParameterBool *p_softClip;

	FFAU::LevelMeterSource* getMeterSourceIn()
	{
		return &meterSourceIn;
	}
	FFAU::LevelMeterSource* getMeterSourceOut()
	{
		return &meterSourceOut;
	}

	double getFreq(float value);

private:
    //==============================================================================
	float m_prevPanL = 0.0f;
	float m_prevPanR = 1.0f;
	float l_prevRampGainL = 1.0f;
	float l_prevRampGainR = 0.0f;
	float r_prevRampGainL = 0.0f;
	float r_prevRampGainR = 1.0f;

	float m_prevPrePan = 0.5f;
	float m_prevPreGainL = 0.5f;
	float m_prevPreGainR = 0.5f;

	float m_gainF = 1.0f;
	float m_gainLF = 1.0f;
	float m_gainRF = 1.0f;
	float m_gainMF = 1.0f;
	float m_gainSF = 1.0f;

	float m_prevGain = 1.0f;
	float m_prevGainL = 1.0f;
	float m_prevGainR = 1.0f;
	float m_prevGainM = 1.0f;
	float m_prevGainS = 1.0f;

	float m_prevGainDB = 0.0f;
	float m_prevGainLDB = 0.0f;
	float m_prevGainRDB = 0.0f;
	float m_prevGainMDB = 0.0f;
	float m_prevGainSDB = 0.0f;

	bool m_firstBuffer = true;

	void convertMono(AudioSampleBuffer &buffer);
	void midSideProcess(AudioSampleBuffer &buffer);
	void clipSamples(AudioSampleBuffer &buffer);
	void dcOffset(AudioSampleBuffer &buffer);
	void channelPans(AudioSampleBuffer &buffer);

	AudioSampleBuffer m_midBuffer;
	AudioSampleBuffer m_sideBuffer;
	AudioSampleBuffer m_tempBuffer;

	float m_prevLowPass = 0.0;
	float m_prevHighPass = 100.0;

	double m_sampleRate = 44100.0f;

	ValueSmoother m_lowPassSmoother;
	ValueSmoother m_highPassSmoother;

	double m_prevInL = 0.0f;
	double m_prevOutL = 0.0f;
	double m_prevInR = 0.0f;
	double m_prevOutR = 0.0f;
	double m_eps; 

	FFAU::LevelMeterSource meterSourceIn;
	FFAU::LevelMeterSource meterSourceOut;

	const int hpforder = HPFORDER;

	bool m_monoSet;

	ATK::InPointerFilter<float> inFilterL;
	ATK::InPointerFilter<float> inFilterL2;
	ATK::InPointerFilter<float> inFilterR;
	ATK::InPointerFilter<float> inFilterR2;
	ATK::IIRFilter<ATK::ButterworthLowPassCoefficients<double> > lowpassFilterL;
	ATK::IIRTDF2Filter<ATK::ButterworthHighPassCoefficients<double> > highpassFilterL;
	ATK::IIRFilter<ATK::ButterworthLowPassCoefficients<double> > lowpassFilterR;
	ATK::IIRTDF2Filter<ATK::ButterworthHighPassCoefficients<double> > highpassFilterR;
	ATK::OutPointerFilter<float> outFilterL;
	ATK::OutPointerFilter<float> outFilterL2;
	ATK::OutPointerFilter<float> outFilterR;
	ATK::OutPointerFilter<float> outFilterR2;

	ATK::PipelineGlobalSinkFilter endpoint;
	ATK::PipelineGlobalSinkFilter endpoint2;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (UtilityAudioProcessor)
};

#endif  // PLUGINPROCESSOR_H_INCLUDED
