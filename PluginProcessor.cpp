#include "PluginProcessor.h"
#include "PluginEditor.h"

#define M_PI 3.14159265358979323846
#define OVERRTTWO 0.70710678118

//==============================================================================
UtilityAudioProcessor::UtilityAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       ), inFilterL(NULL, 1, 0, false), outFilterL(NULL, 1, 0, false), inFilterL2(NULL, 1, 0, false), outFilterL2(NULL, 1, 0, false),
																		  inFilterR(NULL, 1, 0, false), outFilterR(NULL, 1, 0, false), inFilterR2(NULL, 1, 0, false), outFilterR2(NULL, 1, 0, false)
#endif
{
	addParameter(p_panL = new AudioParameterFloat("panL", "PanL", 0.0f, 1.0f, 0.0f));
	addParameter(p_panR = new AudioParameterFloat("panR", "PanR", 0.0f, 1.0f, 1.0f));
	addParameter(p_prePan = new AudioParameterFloat("prePan", "PrePan", 0.0f, 1.0f, 0.5f));
	addParameter(p_input = new AudioParameterInt("input", "Input", 1, 5, 1));
	addParameter(p_mute = new AudioParameterBool("mute", "Mute", false));
	addParameter(p_pad = new AudioParameterBool("pad", "Pad", false));
	addParameter(p_DCoffset = new AudioParameterBool("dcOffset", "DCOffset", false));
	addParameter(p_gain = new AudioParameterFloat("gain", "Gain", -35.0f, 35.0f, 0.0f));
	addParameter(p_pan = new AudioParameterFloat("pan", "Pan", 0.0f, 1.0f, 0.5f));
	addParameter(p_gainL = new AudioParameterFloat("gainL", "GainL", -35.0f, 35.0f, 0.0f));
	addParameter(p_gainR = new AudioParameterFloat("gainR", "GainR", -35.0f, 35.0f, 0.0f));
	addParameter(p_phaseL = new AudioParameterBool("phaseL", "PhaseL", false));
	addParameter(p_phaseR = new AudioParameterBool("phaseR", "PhaseR", false));
	addParameter(p_muteL = new AudioParameterBool("muteL", "MuteL", false));
	addParameter(p_muteR = new AudioParameterBool("muteR", "MuteR", false));
	addParameter(p_gainM = new AudioParameterFloat("gainM", "GainM", -60.0f, 20.0f, 0.0f));
	addParameter(p_gainS = new AudioParameterFloat("gainS", "GainS", -60.0f, 20.0f, 0.0f));
	addParameter(p_width = new AudioParameterFloat("width", "Width", -1.0f, 1.0f, 0.0f));
	addParameter(p_muteM = new AudioParameterBool("muteM", "MuteM", false));
	addParameter(p_muteS = new AudioParameterBool("muteS", "MuteS", false));
	addParameter(p_lowPass = new AudioParameterFloat("lowPass", "LowPass", 0.0f, 100.0f, 100.0f));
	addParameter(p_highPass = new AudioParameterFloat("highPass", "HighPass", 0.0f, 100.0f, 0.0f));
	addParameter(p_hpfOn = new AudioParameterBool("hpfOn", "HPFOn", false));
	addParameter(p_lpfOn = new AudioParameterBool("lpfOn", "LPFOn", false));
	addParameter(p_hardClip = new AudioParameterBool("hardClip", "HardClip", false));
	addParameter(p_softClip = new AudioParameterBool("softClip", "SoftClip", false));

	m_midBuffer = AudioSampleBuffer(1, 0);
	m_sideBuffer = AudioSampleBuffer(1, 0);
	m_tempBuffer = AudioSampleBuffer(1, 0);

	endpoint.add_filter(&outFilterL);
	endpoint.add_filter(&outFilterR);

	endpoint2.add_filter(&outFilterL2);
	endpoint2.add_filter(&outFilterR2);

	lowpassFilterL.set_input_port(0, &inFilterL, 0);
	lowpassFilterR.set_input_port(0, &inFilterR, 0);

	lowpassFilterL.set_cut_frequency(*p_lowPass);
	lowpassFilterL.set_order(2);

	lowpassFilterR.set_cut_frequency(*p_lowPass);
	lowpassFilterR.set_order(2);

	outFilterL.set_input_port(0, &lowpassFilterL, 0);
	outFilterR.set_input_port(0, &lowpassFilterR, 0);

	highpassFilterL.set_input_port(0, &inFilterL2, 0);
	highpassFilterR.set_input_port(0, &inFilterR2, 0);

	highpassFilterL.set_cut_frequency(*p_highPass);
	highpassFilterL.set_order(HPFORDER);

	highpassFilterR.set_cut_frequency(*p_highPass);
	highpassFilterR.set_order(HPFORDER);

	outFilterL2.set_input_port(0, &highpassFilterL, 0);
	outFilterR2.set_input_port(0, &highpassFilterR, 0);
}

UtilityAudioProcessor::~UtilityAudioProcessor()
{
}

//==============================================================================
const String UtilityAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool UtilityAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool UtilityAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

double UtilityAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int UtilityAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int UtilityAudioProcessor::getCurrentProgram()
{
    return 0;
}

void UtilityAudioProcessor::setCurrentProgram (int index)
{
}

const String UtilityAudioProcessor::getProgramName (int index)
{
    return String();
}

void UtilityAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void UtilityAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
	m_firstBuffer = true;

	inFilterL.set_input_sampling_rate(sampleRate);
	inFilterL.set_output_sampling_rate(sampleRate);
	inFilterL2.set_input_sampling_rate(sampleRate);
	inFilterL2.set_output_sampling_rate(sampleRate);

	inFilterR.set_input_sampling_rate(sampleRate);
	inFilterR.set_output_sampling_rate(sampleRate);
	inFilterR2.set_input_sampling_rate(sampleRate);
	inFilterR2.set_output_sampling_rate(sampleRate);

	lowpassFilterL.full_setup();
	lowpassFilterR.full_setup();
	highpassFilterL.full_setup();
	highpassFilterR.full_setup();

	lowpassFilterL.set_input_sampling_rate(sampleRate);
	lowpassFilterL.set_output_sampling_rate(sampleRate);
	highpassFilterL.set_input_sampling_rate(sampleRate);
	highpassFilterL.set_output_sampling_rate(sampleRate);

	lowpassFilterR.set_input_sampling_rate(sampleRate);
	lowpassFilterR.set_output_sampling_rate(sampleRate);
	highpassFilterR.set_input_sampling_rate(sampleRate);
	highpassFilterR.set_output_sampling_rate(sampleRate);

	outFilterL.set_input_sampling_rate(sampleRate);
	outFilterL.set_output_sampling_rate(sampleRate);
	outFilterL2.set_input_sampling_rate(sampleRate);
	outFilterL2.set_output_sampling_rate(sampleRate);

	outFilterR.set_input_sampling_rate(sampleRate);
	outFilterR.set_output_sampling_rate(sampleRate);
	outFilterR2.set_input_sampling_rate(sampleRate);
	outFilterR2.set_output_sampling_rate(sampleRate);

	endpoint.set_input_sampling_rate(sampleRate);
	endpoint.set_output_sampling_rate(sampleRate);

	endpoint2.set_input_sampling_rate(sampleRate);
	endpoint2.set_output_sampling_rate(sampleRate);

	m_midBuffer.setSize(1, samplesPerBlock);
	m_sideBuffer.setSize(1, samplesPerBlock);
	m_tempBuffer.setSize(1, samplesPerBlock);

	//*p_highPass = fmin(static_cast<float>(*p_highPass), sampleRate / 2.0f);
	//*p_lowPass = fmin(static_cast<float>(*p_lowPass), sampleRate / 2.0f);

	lowpassFilterL.set_cut_frequency(getFreq(*p_lowPass));
	highpassFilterL.set_cut_frequency(getFreq(*p_highPass));

	lowpassFilterR.set_cut_frequency(getFreq(*p_lowPass));
	highpassFilterR.set_cut_frequency(getFreq(*p_highPass));

	m_sampleRate = sampleRate;

	m_lowPassSmoother.reset(sampleRate, samplesPerBlock, *p_lowPass, 0.2);
	m_highPassSmoother.reset(sampleRate, samplesPerBlock, *p_highPass, 0.2);

	m_prevHighPass = *p_highPass;
	m_prevLowPass = *p_lowPass;

	/*if (getActiveEditor() != nullptr)
	{
		UtilityAudioProcessorEditor* editor = static_cast<UtilityAudioProcessorEditor*>(getActiveEditor());
		editor->m_highPass.setRange(20.0f, fmin(20000.0f, (sampleRate / 2.0f) - 500.0f), 1.0f);
		editor->m_highPass.setValue(*p_highPass);
		editor->m_lowPass.setRange(20.0f, fmin(20000.0f, (sampleRate / 2.0f) - 500.0f), 1.0f);
		editor->m_lowPass.setValue(*p_lowPass);
	}*/

	meterSourceIn.resize(2, std::max(1.0, 0.2 * sampleRate / samplesPerBlock));
	meterSourceOut.resize(2, std::max(1.0, 0.2 * sampleRate / samplesPerBlock));
}

void UtilityAudioProcessor::releaseResources()
{
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool UtilityAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
	if (layouts.getMainOutputChannels() <= 2 && layouts.getMainOutputChannels() == 2)
	{
		return true;
	}

	return false;
}
#endif

void UtilityAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{

    const int totalNumInputChannels  = getTotalNumInputChannels();
    const int totalNumOutputChannels = getTotalNumOutputChannels();

	if (totalNumInputChannels == 1 && !m_monoSet)
	{
		m_monoSet = true;
		*p_input = 2;
	}

	const int numSamples = buffer.getNumSamples();

    for (int i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, numSamples);

	meterSourceIn.measureBlock(buffer);

	if (*p_lowPass != m_prevLowPass)
	{
		if (*p_lowPass != m_lowPassSmoother.getTarget())
		{
			m_lowPassSmoother.setTarget(*p_lowPass);
		}
		float newVal = m_lowPassSmoother.getNextValue();
		//float newVal = *p_lowPass;
		double newFreq = getFreq(newVal);
		lowpassFilterL.set_cut_frequency(newFreq);
		lowpassFilterR.set_cut_frequency(newFreq);
		m_prevLowPass = newVal;
	}

	if (*p_highPass != m_prevHighPass)
	{
		if (*p_highPass != m_highPassSmoother.getTarget())
		{
			m_highPassSmoother.setTarget(*p_highPass);
		}
		float newVal = m_highPassSmoother.getNextValue();
		//float newVal = *p_highPass;
		double newFreq = getFreq(newVal);
		highpassFilterL.set_cut_frequency(newFreq);
		highpassFilterR.set_cut_frequency(newFreq);
		m_prevHighPass = newVal;
	}

	if (*p_mute)
		buffer.clear();

	if (*p_input == 1)
	{
		if (*p_panL != 0.0f || *p_panR != 1.0f)
			channelPans(buffer);
	}
	else if (*p_input == 2)
	{
		buffer.copyFrom(1, 0, buffer.getReadPointer(0), numSamples);
	}
	else if (*p_input == 3)
	{
		buffer.copyFrom(0, 0, buffer.getReadPointer(1), numSamples);
	}
	else if (*p_input == 4)
	{
		convertMono(buffer);
	}
	else if (*p_input == 5)
	{
		m_tempBuffer.copyFrom(0, 0, buffer.getReadPointer(0), numSamples);
		buffer.copyFrom(0, 0, buffer.getReadPointer(1), numSamples);
		buffer.copyFrom(1, 0, m_tempBuffer.getReadPointer(0), numSamples);
		if (*p_panL != 0.0f || *p_panR != 1.0f)
			channelPans(buffer);
	}

	if (*p_DCoffset)
		dcOffset(buffer);

	//we apply two gains to each channel, one for immediate changes and one for ramped changes

	if (*p_gain != m_prevGainDB) //I do this messy nonsense to mildly save CPU usage
	{
		m_prevGainDB = *p_gain;
		m_gainF = Decibels::decibelsToGain(m_prevGainDB);
	}
	if (*p_gainL != m_prevGainLDB)
	{
		m_prevGainLDB = *p_gainL;
		m_gainLF = Decibels::decibelsToGain(m_prevGainLDB);
	}
	if (*p_gainR != m_prevGainRDB) 
	{
		m_prevGainRDB = *p_gainR;
		m_gainRF = Decibels::decibelsToGain(m_prevGainRDB);
	}
	if (*p_gainM != m_prevGainMDB)
	{
		m_prevGainMDB = *p_gainM;
		m_gainMF = Decibels::decibelsToGain(m_prevGainMDB);
	}
	if (*p_gainS != m_prevGainSDB)
	{
		m_prevGainSDB = *p_gainS;
		m_gainSF = Decibels::decibelsToGain(m_prevGainSDB);
	}

	float rampGainL = m_gainLF * m_gainF;
	float rampGainR = m_gainRF * m_gainF;

	float instantGainL = *p_phaseL ? -1.0f : 1.0f;
	float instantGainR = *p_phaseR ? -1.0f : 1.0f;

	if (*p_width != 0.0f || *p_gainS != 0.0f || *p_gainM != 0.0f || *p_muteS || *p_muteM)
		midSideProcess(buffer);

	if (*p_pan != 0.5f)
	{
		rampGainL *= cos(*p_pan * 2 * M_PI / 4) * 1.414f;
		rampGainR *= sin(*p_pan * 2 * M_PI / 4) * 1.414f;
	}


	if (rampGainL != m_prevGainL)
	{
		if (!m_firstBuffer)
			buffer.applyGainRamp(0, 0, numSamples, m_prevGainL, rampGainL);
		else
			buffer.applyGain(0, 0, numSamples, rampGainL);
		m_prevGainL = rampGainL;
	}
	else
	{
		buffer.applyGain(0, 0, numSamples, rampGainL);
	}

	if (rampGainR != m_prevGainR)
	{
		if (!m_firstBuffer)
			buffer.applyGainRamp(1, 0, numSamples, m_prevGainR, rampGainR);
		else
			buffer.applyGain(1, 0, numSamples, rampGainR);
		m_prevGainR = rampGainR;
	}
	else
	{
		buffer.applyGain(1, 0, numSamples, rampGainR);
	}

	if (*p_hpfOn)
	{
		inFilterL2.set_pointer(buffer.getReadPointer(0), numSamples);
		outFilterL2.set_pointer(buffer.getWritePointer(0), numSamples);

		inFilterR2.set_pointer(buffer.getReadPointer(1), numSamples);
		outFilterR2.set_pointer(buffer.getWritePointer(1), numSamples);
		endpoint2.process(numSamples);
	}
	if (*p_lpfOn)
	{
		inFilterL.set_pointer(buffer.getReadPointer(0), numSamples);
		outFilterL.set_pointer(buffer.getWritePointer(0), numSamples);

		inFilterR.set_pointer(buffer.getReadPointer(1), numSamples);
		outFilterR.set_pointer(buffer.getWritePointer(1), numSamples);
		endpoint.process(numSamples);
	}

	instantGainL *= *p_muteL ? 0.0f : 1.0f;
	instantGainR *= *p_muteR ? 0.0f : 1.0f;

	if (*p_pad)
	{
		instantGainL *= 0.1f;
		instantGainR *= 0.1f;
	}

	buffer.applyGain(0, 0, numSamples, instantGainL);
	buffer.applyGain(1, 0, numSamples, instantGainR);

	if (*p_hardClip || *p_softClip)
		clipSamples(buffer);

	meterSourceOut.measureBlock(buffer);

	m_firstBuffer = false;
}

//==============================================================================
bool UtilityAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* UtilityAudioProcessor::createEditor()
{
    return new UtilityAudioProcessorEditor (*this);
}

//==============================================================================
void UtilityAudioProcessor::getStateInformation (MemoryBlock& destData)
{
	ScopedPointer<XmlElement> xml(new XmlElement("PluginSettings"));
	xml->setAttribute("p_panL", (double)*p_panL);
	xml->setAttribute("p_panR", (double)*p_panR);
	xml->setAttribute("p_prePan", (double)*p_prePan);
	xml->setAttribute("p_input", (int)*p_input);
	xml->setAttribute("p_mute", (bool)*p_mute);
	xml->setAttribute("p_pad", (bool)*p_pad);
	xml->setAttribute("p_DCoffset", (bool)*p_DCoffset);
	xml->setAttribute("p_gain", (double)*p_gain);
	xml->setAttribute("p_pan", (double)*p_pan);
	xml->setAttribute("p_gainL", (double)*p_gainL);
	xml->setAttribute("p_gainR", (double)*p_gainR);
	xml->setAttribute("p_phaseL", (bool)*p_phaseL);
	xml->setAttribute("p_phaseR", (bool)*p_phaseR);
	xml->setAttribute("p_muteL", (bool)*p_muteL);
	xml->setAttribute("p_muteR", (bool)*p_muteR);
	xml->setAttribute("p_gainM", (double)*p_gainM);
	xml->setAttribute("p_gainS", (double)*p_gainS);
	xml->setAttribute("p_width", (double)*p_width);
	xml->setAttribute("p_muteM", (bool)*p_muteM);
	xml->setAttribute("p_muteS", (bool)*p_muteS);
	xml->setAttribute("p_lowPass", (double)*p_lowPass);
	xml->setAttribute("p_highPass", (double)*p_highPass);
	xml->setAttribute("p_hpfOn", (bool)*p_hpfOn);
	xml->setAttribute("p_lpfOn", (bool)*p_lpfOn);
	xml->setAttribute("p_hardClip", (bool)*p_hardClip);
	xml->setAttribute("p_softClip", (bool)*p_softClip);
	copyXmlToBinary(*xml, destData);
}

void UtilityAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
	ScopedPointer<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));
	if (xmlState != nullptr)
		if (xmlState->hasTagName("PluginSettings"))
		{
			*p_panL = xmlState->getDoubleAttribute("p_panL", 0.0f);
			*p_panR = xmlState->getDoubleAttribute("p_panR", 1.0f);
			*p_prePan = xmlState->getDoubleAttribute("p_prePan", 0.5f);
			*p_input = xmlState->getIntAttribute("p_input", 1);
			*p_mute = xmlState->getBoolAttribute("p_mute", false);
			*p_pad = xmlState->getBoolAttribute("p_pad", false);
			*p_DCoffset = xmlState->getBoolAttribute("p_DCoffset", false);
			*p_gain = xmlState->getDoubleAttribute("p_gain", 0.0f);
			*p_pan = xmlState->getDoubleAttribute("p_pan", 0.5f);
			*p_gainL = xmlState->getDoubleAttribute("p_gainL", 0.0f);
			*p_gainR = xmlState->getDoubleAttribute("p_gainR", 0.0f);
			*p_phaseL = xmlState->getBoolAttribute("p_phaseL", false);
			*p_phaseR = xmlState->getBoolAttribute("p_phaseR", false);
			*p_muteL = xmlState->getBoolAttribute("p_muteL", false);
			*p_muteR = xmlState->getBoolAttribute("p_muteR", false);
			*p_gainM = xmlState->getDoubleAttribute("p_gainM", 0.0f);
			*p_gainS = xmlState->getDoubleAttribute("p_gainS", 0.0f);
			*p_width = xmlState->getDoubleAttribute("p_width", 0.0f);
			*p_muteM = xmlState->getBoolAttribute("p_muteM", false);
			*p_muteS = xmlState->getBoolAttribute("p_muteS", false);
			*p_lowPass = xmlState->getDoubleAttribute("p_lowPass", 20000.0f);
			*p_highPass = xmlState->getDoubleAttribute("p_highPass", 20.0f);
			*p_hpfOn = xmlState->getBoolAttribute("p_hpfOn", false);
			*p_lpfOn = xmlState->getBoolAttribute("p_lpfOn", false);
			*p_hardClip = xmlState->getBoolAttribute("p_hardClip", false);
			*p_hardClip = xmlState->getBoolAttribute("p_softClip", false);
		}
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new UtilityAudioProcessor();
}

void UtilityAudioProcessor::convertMono(AudioSampleBuffer & buffer)
{
	if (buffer.getNumChannels() != 2)
		return;

	int numSamples = buffer.getNumSamples();

	float* channelDataL = buffer.getWritePointer(0);
	float* channelDataR = buffer.getWritePointer(1);

	float rampGainL = 0.5f;
	float rampGainR = 0.5f;

	if (*p_prePan != 0.5f)
	{
		//rampGainL *= cos(*p_prePan * 2 * M_PI / 4) * 1.414f;
		//rampGainR *= sin(*p_prePan * 2 * M_PI / 4) * 1.414f;
		rampGainL = 1.0f - *p_prePan;
		rampGainR = *p_prePan;
	}

	if (m_firstBuffer)
	{
		m_prevPrePan = *p_prePan;
		m_prevPreGainL = rampGainL;
		m_prevPreGainR = rampGainR;
	}

	if (*p_prePan != m_prevPrePan)
	{
		float incrementL = (rampGainL - m_prevPreGainL) / numSamples;
		float incrementR = (rampGainR - m_prevPreGainR) / numSamples;
		float currentRampL = m_prevPreGainL;
		float currentRampR = m_prevPreGainR;

		for (int i = 0; i < buffer.getNumSamples(); i++)
		{

			currentRampL += incrementL;
			currentRampR += incrementR;

			float avgSum = currentRampL*channelDataL[i] + currentRampR*channelDataR[i];
			channelDataL[i] = avgSum;
			channelDataR[i] = avgSum;
		}
		m_prevPrePan = *p_prePan;
		m_prevPreGainL = currentRampL;
		m_prevPreGainR = currentRampR;
	}
	else
	{
		for (int i = 0; i < buffer.getNumSamples(); i++)
		{
			float avgSum = rampGainL*channelDataL[i] + rampGainR*channelDataR[i];
			channelDataL[i] = avgSum;
			channelDataR[i] = avgSum;
		}
	}
}

void UtilityAudioProcessor::midSideProcess(AudioSampleBuffer & buffer)
{
	int numSamples = buffer.getNumSamples();

	float *channelDataL = buffer.getWritePointer(0);
	float *channelDataR = buffer.getWritePointer(1);
	float *midData = m_midBuffer.getWritePointer(0);
	float *sideData = m_sideBuffer.getWritePointer(0);

	float finalGainM = m_gainMF;
	float finalGainS = m_gainSF;

	if (*p_width < 0.0f)
	{
		finalGainS *= 1.0f + *p_width;
	}
	else if (*p_width > 0.0f)
	{
		finalGainM *= 1.0f - *p_width;
	}

	finalGainM *= *p_muteM ? 0.0f : 1.0f;
	finalGainS *= *p_muteS ? 0.0f : 1.0f;

	for (int i = 0; i < numSamples; i++)
	{
		midData[i] = (channelDataL[i] + channelDataR[i]) * OVERRTTWO;
		sideData[i] = (channelDataL[i] - channelDataR[i]) * OVERRTTWO;
	}

	if (m_firstBuffer)
	{
		m_prevGainM = finalGainM;
		m_prevGainS = finalGainS;
	}

	if (finalGainM != m_prevGainM)
	{
		m_midBuffer.applyGainRamp(0, numSamples, m_prevGainM, finalGainM);
		m_prevGainM = finalGainM;
	}
	else
	{
		m_midBuffer.applyGain(finalGainM);
	}

	if (finalGainS != m_prevGainS)
	{
		m_sideBuffer.applyGainRamp(0, numSamples, m_prevGainS, finalGainS);
		m_prevGainS = finalGainS;
	}
	else
	{
		m_sideBuffer.applyGain(finalGainS);
	}

	for (int i = 0; i < buffer.getNumSamples(); i++)
	{
		channelDataL[i] = (midData[i] + sideData[i]) * OVERRTTWO;
		channelDataR[i] = (midData[i] - sideData[i]) * OVERRTTWO;
	}
}

template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

void UtilityAudioProcessor::clipSamples(AudioSampleBuffer & buffer)
{
	int numSamples = buffer.getNumSamples();
	int numChans = buffer.getNumChannels();

	if (*p_hardClip)
	{
		for (int j = 0; j < numChans; j++)
		{
			float *bufferData = buffer.getWritePointer(j);

			for (int i = 0; i < buffer.getNumSamples(); i++)
			{
				float s = bufferData[i];
				bufferData[i] = sgn(s)*fmin(fabs(s), 1.0f);
			}
		}
	}
	else if (*p_softClip)
	{
		for (int j = 0; j < numChans; j++)
		{
			float *bufferData = buffer.getWritePointer(j);

			for (int i = 0; i < buffer.getNumSamples(); i++)
			{
				float s = bufferData[i];
				if (fabs(s) < 1.5f)
					bufferData[i] = s - (4.0f / 27.0f)*powf(s, 3.0f);
				else
					bufferData[i] = sgn(s)*1.0f;
			}
		}
	}
}

void UtilityAudioProcessor::dcOffset(AudioSampleBuffer & buffer)
{
	int numSamples = buffer.getNumSamples();
	int numChans = buffer.getNumChannels();

	if (m_sampleRate == 44100.0) ////use eps = 2*pi*f/fs where f is the cutoff and fs is sample rate, storing for faster lookup
	{
		m_eps = 0.00142475857;
	}
	else if (m_sampleRate == 48000.0)
	{
		m_eps = 0.00130899693;
	}
	else if (m_sampleRate == 88200.0)
	{
		m_eps = 0.00071237928;
	}
	else if (m_sampleRate == 96000.0)
	{
		m_eps = 0.00065449846;
	}
	else
	{
		m_eps = 0.00142475857;
	}

	double sampleL = 0.0;
	double sampleR = 0.0;

	float *bufferDataL = buffer.getWritePointer(0);
	float *bufferDataR = buffer.getWritePointer(1);
	for (int i = 0; i < numSamples; i++)
	{
		sampleL = bufferDataL[i];
		bufferDataL[i] = (1 - m_eps) * m_prevOutL + sampleL - m_prevInL;
		m_prevOutL = bufferDataL[i];
		m_prevInL = sampleL;

		sampleR = bufferDataR[i];
		bufferDataR[i] = (1 - m_eps) * m_prevOutR + sampleR - m_prevInR;
		m_prevOutR = bufferDataR[i];
		m_prevInR = sampleR;
	}
}

void UtilityAudioProcessor::channelPans(AudioSampleBuffer & buffer)
{
	int numSamples = buffer.getNumSamples();

	float *channelDataL = buffer.getWritePointer(0);
	float *channelDataR = buffer.getWritePointer(1);

	float l_rampGainL = cos(*p_panL * 2 * M_PI / 4); //* 1.414f;
	float l_rampGainR = sin(*p_panL * 2 * M_PI / 4); //* 1.414f;

	float r_rampGainL = cos(*p_panR * 2 * M_PI / 4); //* 1.414f;
	float r_rampGainR = sin(*p_panR * 2 * M_PI / 4); //* 1.414f;

	if (m_firstBuffer)
	{
		l_prevRampGainL = l_rampGainL;
		l_prevRampGainR = l_rampGainR;
		r_prevRampGainL = r_rampGainL;
		r_prevRampGainR = r_rampGainR;

		m_prevPanL = *p_panL;
		m_prevPanR = *p_panR;
	}

	if (*p_panL != m_prevPanL || *p_panR != m_prevPanR)
	{
		float l_incrementL = (l_rampGainL - l_prevRampGainL) / numSamples;
		float l_incrementR = (l_rampGainR - l_prevRampGainR) / numSamples;
		float r_incrementL = (r_rampGainL - r_prevRampGainL) / numSamples;
		float r_incrementR = (r_rampGainR - r_prevRampGainR) / numSamples;
		float l_currentRampL = l_prevRampGainL;
		float l_currentRampR = l_prevRampGainR;
		float r_currentRampL = r_prevRampGainL;
		float r_currentRampR = r_prevRampGainR;

		float newL;
		float newR;

		for (int i = 0; i < numSamples; i++)
		{
			l_currentRampL += l_incrementL;
			l_currentRampR += l_incrementR;
			r_currentRampL += r_incrementL;
			r_currentRampR += r_incrementR;
			newL = channelDataL[i] * l_currentRampL + channelDataR[i] *  r_currentRampL;
			newR = channelDataL[i] * l_currentRampR + channelDataR[i] * r_currentRampR;
			channelDataL[i] = newL;
			channelDataR[i] = newR;
		}

		l_prevRampGainL = l_rampGainL;
		l_prevRampGainR = l_rampGainR;
		r_prevRampGainL = r_rampGainL;
		r_prevRampGainR = r_rampGainR;

		m_prevPanL = *p_panL;
		m_prevPanR = *p_panR;
	}
	else
	{
		float newL;
		float newR;
		for (int i = 0; i < numSamples; i++)
		{
			newL = channelDataL[i] * l_rampGainL + channelDataR[i] * r_rampGainL;
			newR = channelDataL[i] * l_rampGainR + channelDataR[i] * r_rampGainR;
			channelDataL[i] = newL;
			channelDataR[i] = newR;
		}
	}
}

double UtilityAudioProcessor::getFreq(float value)
{
	//return fmin(20.0 + 0.0002 * pow((double)value, 4.0), (m_sampleRate / 2.0) - 500.0);
	return fmin(exp(2.99573227355 + 0.01 * value * 6.90775527898), (m_sampleRate / 2.0) - 500.0);
}

ValueSmoother::ValueSmoother()
{
	m_sampleRate = 0.0;
	m_samplesPerBlock = 0;
	m_currentValue = 0.0f;
	m_time = 0.0;
	m_target = 0.0f;
	m_interval = 0.0f;
}

ValueSmoother::~ValueSmoother()
{
}

void ValueSmoother::reset(double sampleRate, int samplesPerBlock, double initValue, double time)
{
	m_sampleRate = sampleRate;
	m_samplesPerBlock = samplesPerBlock;
	m_currentValue = initValue;
	m_time = time;
}

void ValueSmoother::setTarget(double target)
{
	m_target = target;
	m_interval = (m_target - m_currentValue) / (m_time *(m_sampleRate / m_samplesPerBlock));
}

float ValueSmoother::getNextValue()
{
	if (m_currentValue != m_target)
	{
		if ((m_interval > 0 && m_target < m_currentValue) || (m_interval < 0 && m_target > m_currentValue))
			m_currentValue = m_target;
		else
			m_currentValue += m_interval;
	}
	return m_currentValue;
}

float ValueSmoother::getTarget()
{
	return m_target;
}
