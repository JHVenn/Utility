# Utility

Utility is a free, lightweight, universal general purpose all in one digital channel strip plugin offering precise controls over various aspects of an audio signal, from mid/side processing to soft clipping.
Features include:

- input routing with separate pans for the left and right channels, or alternatively a pre mono balance control to decide how the left and right channels are blended for conversion into mono
- gain, and individual left and right channel gains, mutes
- pan and phase inversion of either channel
- width control, along with separate mid and side gain sliders and mutes
- maximally flat minimal phase high and low pass 12db/octave filtering
- optional hard and soft clipping at 0db
- everything is fully automatable with sensible parameter ranges/scaling

### Compiling

- Utility uses [Juce](https://www.juce.com/) v4+ framework. You may generate a new JUCE plugin project in Projucer, and replace the source code files with the Utility source, remmeber to also add the source direcotry to the includes path in your IDE.
- Utility requires Matthieu Brucher's Audio Toolkit, you can find precompiled libraries to link to on the releases tab: https://github.com/mbrucher/AudioTK
- In this repo exists a "ff-meter" folder, this is a specific version of Foley's Finest "ff_meters" library, which must be added as a JUCE module (not #included) in Projucer.
- If you're building on windows, you will also need the VST3 SDK, which you can download from from the [Steinberg SDK download portal](http://www.steinberg.net/nc/en/company/developers/sdk_download_portal.html).

### Credits
- Utility was developed by Jonathan Hyde of [Venn Audio](http://www.vennaudio.com/) and [Small Ocean](http://www.smallocean.net)

##### Thanks to:
- Matthieu Brucher for his Audio Toolkit library
- Daniel Walz of Foley's Finest, for the FF-Meters module.

### License
- Utility is licensed under the terms of the GNU General Public License Version 3

