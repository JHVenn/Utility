﻿#include "PluginProcessor.h"
#include "PluginEditor.h"
#include <iomanip>
#include <sstream>

#define BARC 50, 50, 50
#define BGC 210, 210, 210

//==============================================================================
UtilityAudioProcessorEditor::UtilityAudioProcessorEditor (UtilityAudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
	vlogo = ImageCache::getFromMemory(BinaryData::vlogo_png, BinaryData::vlogo_pngSize);

	m_input.addItem("Stereo", 1);
	m_input.addItem("Left", 2);
	m_input.addItem("Right", 3);
	m_input.addItem("Dual Mono", 4);
	m_input.addItem("Swap", 5);
	m_input.setJustificationType(Justification::centred);
	m_input.setSelectedId(*processor.p_input, NotificationType::dontSendNotification);
	m_input.addListener(this);
	addAndMakeVisible(&m_input);

	m_mute.setClickingTogglesState(true);
	m_mute.setButtonText("Mute");
	m_mute.setColour(m_mute.buttonOnColourId, Colours::red);
	m_mute.addListener(this);
	m_mute.setToggleState(*processor.p_mute, NotificationType::dontSendNotification);
	addAndMakeVisible(&m_mute);

	m_pad.setClickingTogglesState(true);
	m_pad.setButtonText("Pad");
	m_pad.setColour(m_pad.buttonOnColourId, Colours::green);
	m_pad.addListener(this);
	m_pad.setToggleState(*processor.p_pad, NotificationType::dontSendNotification);
	addAndMakeVisible(&m_pad);

	m_DCoffset.setClickingTogglesState(true);
	m_DCoffset.setButtonText("DC");
	m_DCoffset.setColour(m_DCoffset.buttonOnColourId, Colours::lightgreen);
	m_DCoffset.addListener(this);
	m_DCoffset.setToggleState(*processor.p_DCoffset, NotificationType::dontSendNotification);
	addAndMakeVisible(&m_DCoffset);

	m_phaseL.setClickingTogglesState(true);
	m_phaseL.setButtonText(CharPointer_UTF8("\xc3\x98"));
	m_phaseL.setColour(m_phaseL.buttonOnColourId, Colours::lightgreen);
	m_phaseL.setToggleState(*processor.p_phaseL, NotificationType::dontSendNotification);
	m_phaseR.setClickingTogglesState(true);
	m_phaseR.setButtonText(CharPointer_UTF8("\xc3\x98"));
	m_phaseR.setColour(m_phaseR.buttonOnColourId, Colours::lightgreen);
	m_phaseR.setToggleState(*processor.p_phaseR, NotificationType::dontSendNotification);
	m_phaseL.addListener(this);
	m_phaseR.addListener(this);
	addAndMakeVisible(&m_phaseL);
	addAndMakeVisible(&m_phaseR);

	m_muteL.setClickingTogglesState(true);
	m_muteL.setButtonText("L");
	m_muteL.setColour(m_muteL.buttonColourId, Colours::lightgreen);
	m_muteL.setColour(m_muteL.buttonOnColourId, Colours::lightgrey);
	m_muteL.setToggleState(*processor.p_muteL, NotificationType::dontSendNotification);
	m_muteR.setClickingTogglesState(true);
	m_muteR.setButtonText("R");
	m_muteR.setColour(m_muteR.buttonColourId, Colours::lightgreen);
	m_muteR.setColour(m_muteR.buttonOnColourId, Colours::lightgrey);
	m_muteL.setToggleState(*processor.p_muteL, NotificationType::dontSendNotification);
	m_muteL.addListener(this);
	m_muteR.addListener(this);
	addAndMakeVisible(&m_muteL);
	addAndMakeVisible(&m_muteR);

	m_muteM.setClickingTogglesState(true);
	m_muteM.setButtonText("M");
	m_muteM.setColour(m_muteM.buttonColourId, Colours::lightgreen);
	m_muteM.setColour(m_muteM.buttonOnColourId, Colours::lightgrey);
	m_muteM.setToggleState(*processor.p_muteM, NotificationType::dontSendNotification);
	m_muteS.setClickingTogglesState(true);
	m_muteS.setButtonText("S");
	m_muteS.setColour(m_muteS.buttonColourId, Colours::lightgreen);
	m_muteS.setColour(m_muteS.buttonOnColourId, Colours::lightgrey);
	m_muteS.setToggleState(*processor.p_muteS, NotificationType::dontSendNotification);
	m_muteM.addListener(this);
	m_muteS.addListener(this);
	addAndMakeVisible(&m_muteM);
	addAndMakeVisible(&m_muteS);

	m_hpfOn.setClickingTogglesState(true);
	m_hpfOn.setButtonText("HPF");
	m_hpfOn.setColour(m_mute.buttonOnColourId, Colours::yellow);
	m_hpfOn.setToggleState(*processor.p_hpfOn, NotificationType::dontSendNotification);
	m_lpfOn.setClickingTogglesState(true);
	m_lpfOn.setButtonText("LPF");
	m_lpfOn.setColour(m_mute.buttonOnColourId, Colours::yellow);
	m_lpfOn.setToggleState(*processor.p_lpfOn, NotificationType::dontSendNotification);
	m_hpfOn.addListener(this);
	m_lpfOn.addListener(this);
	addAndMakeVisible(&m_hpfOn);
	addAndMakeVisible(&m_lpfOn);

	m_hardClip.setClickingTogglesState(true);
	m_hardClip.setButtonText("H.Clip");
	m_hardClip.setColour(m_hardClip.buttonOnColourId, Colours::lightgreen);
	m_hardClip.setToggleState(*processor.p_hardClip, NotificationType::dontSendNotification);
	m_softClip.setClickingTogglesState(true);
	m_softClip.setButtonText("S.Clip");
	m_softClip.setColour(m_softClip.buttonOnColourId, Colours::lightgreen);
	m_softClip.setToggleState(*processor.p_softClip, NotificationType::dontSendNotification);
	m_hardClip.addListener(this);
	m_softClip.addListener(this);
	addAndMakeVisible(&m_hardClip);
	addAndMakeVisible(&m_softClip);

	m_panL.setSliderStyle(Slider::RotaryVerticalDrag);
	m_panL.setRange(0.0f, 1.0f, 0.005f);
	m_panL.setMouseCursor(MouseCursor::UpDownResizeCursor);
	m_panL.setVelocityBasedMode(false);
	m_panL.setVelocityModeParameters(0.5, 2, 0.01);
	m_panL.setValue(*processor.p_panL, NotificationType::dontSendNotification);
	m_panL.setDoubleClickReturnValue(true, 0.0f);
	m_panL.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_panL.setLookAndFeel(&otherLookAndFeel2);
	m_panL.addListener(this);

	m_panR.setSliderStyle(Slider::RotaryVerticalDrag);
	m_panR.setRange(0.0f, 1.0f, 0.005f);
	m_panR.setMouseCursor(MouseCursor::UpDownResizeCursor);
	m_panR.setVelocityBasedMode(false);
	m_panR.setVelocityModeParameters(0.5, 2, 0.01);
	m_panR.setValue(*processor.p_panR, NotificationType::dontSendNotification);
	m_panR.setDoubleClickReturnValue(true, 1.0f);
	m_panR.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_panR.setLookAndFeel(&otherLookAndFeel2);
	m_panR.addListener(this);

	m_prePan.setSliderStyle(Slider::RotaryVerticalDrag);
	m_prePan.setRange(0.0f, 1.0f, 0.005f);
	m_prePan.setMouseCursor(MouseCursor::UpDownResizeCursor);
	m_prePan.setVelocityBasedMode(false);
	m_prePan.setVelocityModeParameters(0.5, 2, 0.01);
	m_prePan.setValue(*processor.p_prePan, NotificationType::dontSendNotification);
	m_prePan.setDoubleClickReturnValue(true, 0.5f);
	m_prePan.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_prePan.setLookAndFeel(&otherLookAndFeel2);
	m_prePan.addListener(this);

	m_lpanL.setJustificationType(Justification::centred);
	m_lpanL.setFont(12);
	getLPanText();

	m_lpanR.setJustificationType(Justification::centred);
	m_lpanR.setFont(12);
	getRPanText();

	m_lprePan.setJustificationType(Justification::centred);
	m_lprePan.setFont(12);
	getPrePanText();

	m_lchannel.setJustificationType(Justification::centred);

	addChildComponent(&m_panL);
	addChildComponent(&m_panR);
	addChildComponent(&m_prePan);

	addChildComponent(&m_lpanL);
	addChildComponent(&m_lpanR);
	addChildComponent(&m_lprePan);
	addChildComponent(&m_lchannel);

	if (*processor.p_input == 1 || *processor.p_input == 5)
	{
		m_panL.setVisible(true);
		m_panR.setVisible(true);

		m_lpanL.setVisible(true);
		m_lpanR.setVisible(true);
	}
	else if (*processor.p_input == 4)
	{
		m_prePan.setVisible(true);

		m_lprePan.setVisible(true);
	}
	else if (*processor.p_input == 2)
	{
		m_lchannel.setText("Left channel only", NotificationType::dontSendNotification);
		m_lchannel.setVisible(true);
	}
	else if (*processor.p_input == 3)
	{
		m_lchannel.setText("Right channel only", NotificationType::dontSendNotification);
		m_lchannel.setVisible(true);
	}

	m_gain.setSliderStyle(Slider::RotaryVerticalDrag);
	m_gain.setRange(-35.0f, 35.0f, 0.1f);
	m_gain.setMouseCursor(MouseCursor::UpDownResizeCursor);
	m_gain.setVelocityBasedMode(false);
	m_gain.setVelocityModeParameters(0.5, 2, 0.01);
	m_gain.setValue(*processor.p_gain, NotificationType::dontSendNotification);
	m_gain.setDoubleClickReturnValue(true, 0.0f);
	m_gain.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_gain.setLookAndFeel(&otherLookAndFeel);
	addAndMakeVisible(&m_gain);
	m_gain.addListener(this);

	m_pan.setSliderStyle(Slider::RotaryVerticalDrag);
	m_pan.setRange(0.0f, 1.0f, 0.005f);
	m_pan.setMouseCursor(MouseCursor::UpDownResizeCursor);
	m_pan.setVelocityBasedMode(false);
	m_pan.setVelocityModeParameters(0.5, 2, 0.01);
	m_pan.setValue(*processor.p_pan, NotificationType::dontSendNotification);
	m_pan.setDoubleClickReturnValue(true, 0.5f);
	m_pan.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_pan.setLookAndFeel(&otherLookAndFeel2);
	addAndMakeVisible(&m_pan);
	m_pan.addListener(this);

	m_gainL.setSliderStyle(Slider::LinearVertical);
	m_gainL.setRange(-35.0f, 35.0f, 0.1f);
	m_gainL.setVelocityBasedMode(false);
	m_gainL.setVelocityModeParameters(0.5, 2, 0.01);
	m_gainL.setValue(*processor.p_gainL, NotificationType::dontSendNotification);
	m_gainL.setDoubleClickReturnValue(true, 0.0f);
	m_gainL.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_gainL.setPopupDisplayEnabled(true, this);
	m_gainL.setTextValueSuffix(" db");
	addAndMakeVisible(&m_gainL);
	m_gainL.addListener(this);

	m_gainR.setSliderStyle(Slider::LinearVertical);
	m_gainR.setRange(-35.0f, 35.0f, 0.1f);
	m_gainR.setVelocityBasedMode(false);
	m_gainR.setVelocityModeParameters(0.5, 2, 0.01);
	m_gainR.setValue(*processor.p_gainR, NotificationType::dontSendNotification);
	m_gainR.setDoubleClickReturnValue(true, 0.0f);
	m_gainR.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_gainR.setPopupDisplayEnabled(true, this);
	m_gainR.setTextValueSuffix(" db");
	addAndMakeVisible(&m_gainR);
	m_gainR.addListener(this);

	m_gainM.setSliderStyle(Slider::LinearVertical);
	m_gainM.setRange(-60.0f, 20.0f, 0.1f);
	m_gainM.setVelocityBasedMode(false);
	m_gainM.setVelocityModeParameters(0.5, 2, 0.01);
	m_gainM.setValue(*processor.p_gainM, NotificationType::dontSendNotification);
	m_gainM.setDoubleClickReturnValue(true, 0.0f);
	m_gainM.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_gainM.setPopupDisplayEnabled(true, this);
	m_gainM.setTextValueSuffix(" db");
	addAndMakeVisible(&m_gainM);
	m_gainM.addListener(this);

	m_gainS.setSliderStyle(Slider::LinearVertical);
	m_gainS.setRange(-60.0f, 20.0f, 0.1f);
	m_gainS.setVelocityBasedMode(false);
	m_gainS.setVelocityModeParameters(0.5, 2, 0.01);
	m_gainS.setValue(*processor.p_gainS, NotificationType::dontSendNotification);
	m_gainS.setDoubleClickReturnValue(true, 0.0f);
	m_gainS.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_gainS.setPopupDisplayEnabled(true, this);
	m_gainS.setTextValueSuffix(" db");
	addAndMakeVisible(&m_gainS);
	m_gainS.addListener(this);

	m_width.setSliderStyle(Slider::RotaryVerticalDrag);
	m_width.setRange(-1.0f, 1.0f, 0.01f);
	m_width.setMouseCursor(MouseCursor::UpDownResizeCursor);
	m_width.setVelocityBasedMode(false);
	m_width.setVelocityModeParameters(0.5, 2, 0.01);
	m_width.setValue(*processor.p_width, NotificationType::dontSendNotification);
	m_width.setDoubleClickReturnValue(true, 0.0f);
	m_width.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	m_width.setLookAndFeel(&otherLookAndFeel3);
	addAndMakeVisible(&m_width);
	m_width.addListener(this);

	m_highPass.setSliderStyle(Slider::RotaryVerticalDrag);
	m_highPass.setRange(0.0, 100.0, 0.01);
	m_highPass.setMouseCursor(MouseCursor::UpDownResizeCursor);
	m_highPass.setVelocityBasedMode(false);
	m_highPass.setVelocityModeParameters(0.5, 2, 0.01);
	m_highPass.setValue(*processor.p_highPass, NotificationType::dontSendNotification);
	m_highPass.setDoubleClickReturnValue(true, 0.5f);
	m_highPass.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	//m_highPass.setSkewFactor(0.35);
	m_highPass.setLookAndFeel(&otherLookAndFeel4);
	m_highPass.setColour(m_highPass.rotarySliderFillColourId, Colour(15, 58, 80));
	m_highPass.setDoubleClickReturnValue(true, 0.0f);
	addAndMakeVisible(m_highPass);
	m_highPass.addListener(this);
	if (*processor.p_hpfOn == false)
		m_highPass.setEnabled(false);

	m_lowPass.setSliderStyle(Slider::RotaryVerticalDrag);
	m_lowPass.setRange(0.0, 100.0, 0.01);
	m_lowPass.setMouseCursor(MouseCursor::UpDownResizeCursor);
	m_lowPass.setVelocityBasedMode(false);
	m_lowPass.setVelocityModeParameters(0.5, 2, 0.01);
	m_lowPass.setValue(*processor.p_lowPass, NotificationType::dontSendNotification);
	m_lowPass.setDoubleClickReturnValue(true, 0.5f);
	m_lowPass.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	//m_lowPass.setSkewFactor(0.35);
	m_lowPass.setColour(m_lowPass.rotarySliderFillColourId, Colour(15, 58, 80));
	m_lowPass.setDoubleClickReturnValue(true, fmin(20000.0f, processor.getSampleRate() / 2.0f));
	addAndMakeVisible(m_lowPass);
	m_lowPass.addListener(this);
	if (*processor.p_lpfOn == false)
		m_lowPass.setEnabled(false);

	m_lgainDB.setJustificationType(Justification::centred);
	addAndMakeVisible(&m_lgainDB);
	getGainDB();

	m_lPan.setJustificationType(Justification::centred);
	addAndMakeVisible(&m_lPan);
	getPanText();

	m_lWidth.setJustificationType(Justification::centred);
	m_lWidth.setFont(12);
	addAndMakeVisible(&m_lWidth);
	getWidthText();

	m_lLowpass.setJustificationType(Justification::centred);
	m_lLowpass.setFont(12);
	addAndMakeVisible(&m_lLowpass);

	m_lHighpass.setJustificationType(Justification::centred);
	m_lHighpass.setFont(12);
	addAndMakeVisible(&m_lHighpass);

	m_lLowpass.setText(static_cast<String>(round(processor.getFreq(m_lowPass.getValue()))) + "hz", NotificationType::dontSendNotification);
	m_lHighpass.setText(static_cast<String>(round(processor.getFreq(m_highPass.getValue()))) + "hz", NotificationType::dontSendNotification);

	m_lLowpass.setEnabled(*processor.p_lpfOn);
	m_lHighpass.setEnabled(*processor.p_hpfOn);

	lnfIn = new FFAU::LevelMeterLookAndFeel();
	lnfOut = new FFAU::LevelMeterLookAndFeel();

	lnfIn->setColour(FFAU::LevelMeter::lmMeterGradientLowColour, juce::Colours::green);
	lnfOut->setColour(FFAU::LevelMeter::lmMeterGradientLowColour, juce::Colours::green);

	meterIn = new FFAU::LevelMeter(FFAU::LevelMeter::MaxNumber | FFAU::LevelMeter::Minimal);
	meterIn->setLookAndFeel(lnfIn);
	meterIn->setMeterSource(processor.getMeterSourceIn());
	meterOut = new FFAU::LevelMeter(FFAU::LevelMeter::MaxNumber | FFAU::LevelMeter::Minimal);
	meterOut->setLookAndFeel(lnfOut);
	meterOut->setMeterSource(processor.getMeterSourceOut());
	addAndMakeVisible(meterIn);
	addAndMakeVisible(meterOut);

	meterIn->addListener(this);
	meterOut->addListener(this);

	aboutWindow = new AboutWindow();
	aboutWindow->setAlwaysOnTop(true);
	addChildComponent(aboutWindow);

	aboutButton.setButtonText("?");
	aboutButton.addListener(this);
	addAndMakeVisible(&aboutButton);

    setSize (400, 550);
	startTimer(100);
}

UtilityAudioProcessorEditor::~UtilityAudioProcessorEditor()
{
}

//==============================================================================
void UtilityAudioProcessorEditor::paint (Graphics& g)
{
    g.fillAll (Colour(BGC));
	g.setColour(Colour(BARC));
	g.fillRect(horShift + 0, 0, 200, 43);
	g.drawImage(vlogo, 55 + horShift, 9, 25, 25, 0, 0, vlogo.getWidth(), vlogo.getHeight());
	g.fillRect(0, 457 + 50, 100, 43);
	g.fillRect(300, 457 + 50, 100, 43);
	g.setColour(Colour(203, 209, 219));
	g.fillRect(horShift, 43, 200, 104);
	g.setColour(Colour(183, 189, 199));
	g.fillRect(horShift, 147, 200, 1);
	g.setColour(Colours::white);
	g.setFont(16);
	g.drawFittedText("Utility", horShift + 10, 0, 200, 43, Justification::centred, 1);
	g.drawFittedText("IN", 0, 457 + 50, 100, 43, Justification::centred, 1);
	g.drawFittedText("OUT", 300, 457 + 50, 100, 43, Justification::centred, 1);
	g.setColour(Colours::black);
	if (*processor.p_input == 1 || *processor.p_input == 5)
	{
		g.drawFittedText("Left Pan", horShift + 18, 55, 55, 10, Justification::centred, 1);
		g.drawFittedText("Right Pan", horShift + 118, 55, 55, 10, Justification::centred, 1);
	}
	else if (*processor.p_input == 4)
		g.drawFittedText("Pre Mono Balance", horShift, 55, 200, 10, Justification::centred, 1);
	g.drawFittedText("Gain", horShift + 0, vertShift + 180, 200, 10, Justification::centred, 1);
	g.drawFittedText("Pan", horShift + 0, vertShift + 252, 200, 10, Justification::centred, 1);
	g.drawFittedText("Width", horShift + 0, vertShift + 350, 200, 10, Justification::centred, 1);
}

void UtilityAudioProcessorEditor::resized()
{

	m_panL.setBounds(horShift + 30, 70, 30, 30);
	m_lpanL.setBounds(horShift + 17, 93, 60, 24);

	m_panR.setBounds(horShift + 130, 70, 30, 30);
	m_lpanR.setBounds(horShift + 117, 93, 60, 24);

	m_prePan.setBounds(horShift + 77, 67, 34, 34);
	m_lprePan.setBounds(horShift + 64, 93, 60, 24);

	m_lchannel.setBounds(horShift, 67, 200, 24);

	m_mute.setBounds(horShift + 30, vertShift + 90, 40, 24);
	m_pad.setBounds(horShift + 80, vertShift + 90, 40, 24);
	m_DCoffset.setBounds(horShift + 130, vertShift + 90, 40, 24);

	m_gain.setBounds(horShift + 75, vertShift + 130, 50, 50);

	m_pan.setBounds(horShift + 80, vertShift + 210, 40, 40);

	m_gainL.setBounds(horShift + 44, vertShift + 130, 24, 90);
	m_gainR.setBounds(horShift + 131, vertShift + 130, 24, 90);

	m_phaseL.setBounds(horShift + 47, vertShift + 242, 18, 18);
	m_phaseR.setBounds(horShift + 134, vertShift + 242, 18, 18);

	m_muteL.setBounds(horShift + 47, vertShift + 222, 18, 18);
	m_muteR.setBounds(horShift + 134, vertShift + 222, 18, 18);

	m_input.setBounds(horShift + 20, vertShift + 50, 160, 20);

	m_gainM.setBounds(horShift + 54, vertShift + 280, 24, 90);
	m_gainS.setBounds(horShift + 121, vertShift + 280, 24, 90);
	m_width.setBounds(horShift + 80, vertShift + 300, 40, 40);

	m_muteM.setBounds(horShift + 57, vertShift + 372, 18, 18);
	m_muteS.setBounds(horShift + 124, vertShift + 372, 18, 18);

	m_highPass.setBounds(horShift + 5 + 10, vertShift + 305, 30, 30);
	m_lowPass.setBounds(horShift + 155, vertShift + 305, 30, 30);

	m_hpfOn.setBounds(horShift + 13 + 5, vertShift + 345, 24, 18);
	m_lpfOn.setBounds(horShift + 163 - 5, vertShift + 345, 24, 18);

	m_hardClip.setBounds(horShift + 44, vertShift + 410, 44, 24);
	m_softClip.setBounds(horShift + 111, vertShift + 410, 44, 24);

	m_lgainDB.setBounds(horShift + 73, vertShift + 190, 60, 24);
	m_lPan.setBounds(horShift + 73, vertShift + 260, 60, 24);
	m_lWidth.setBounds(horShift + 73, vertShift + 360, 55, 24);
	m_lHighpass.setBounds(horShift + 3, vertShift + 360, 55, 24);
	m_lLowpass.setBounds(horShift + 145, vertShift + 360, 55, 24);

	meterOut->setBounds(horShift + 200, 0, 100, 550 - 43);
	meterIn->setBounds(0, 0, 100, 550 - 43);

	aboutButton.setBounds(getWidth() / 2 - 8, getHeight() - 40, 15, 15);

	aboutWindow->setBounds(85, 135, 230, 230);
}

void UtilityAudioProcessorEditor::buttonClicked(juce::Button * button)
{
	if (button == &m_mute)
	{
		*processor.p_mute = m_mute.getToggleState();
	}
	else if (button == &m_pad)
	{
		*processor.p_pad = m_pad.getToggleState();
	}
	else if (button == &m_DCoffset)
	{
		*processor.p_DCoffset = m_DCoffset.getToggleState();
	}
	else if (button == &m_phaseL)
	{
		*processor.p_phaseL = m_phaseL.getToggleState();
	}
	else if (button == &m_phaseR)
	{
		*processor.p_phaseR = m_phaseR.getToggleState();
	}
	else if (button == &m_muteL)
	{
		*processor.p_muteL = m_muteL.getToggleState();
	}
	else if (button == &m_muteR)
	{
		*processor.p_muteR = m_muteR.getToggleState();
	}
	else if (button == &m_muteM)
	{
		*processor.p_muteM = m_muteM.getToggleState();
	}
	else if (button == &m_muteS)
	{
		*processor.p_muteS = m_muteS.getToggleState();
	}
	else if (button == &m_hpfOn)
	{
		*processor.p_hpfOn = m_hpfOn.getToggleState();
		if (m_hpfOn.getToggleState() == true)
		{
			m_highPass.setEnabled(true);
			m_lHighpass.setText(static_cast<String>(round(processor.getFreq(m_highPass.getValue()))) + "hz", NotificationType::dontSendNotification);
		}
		else
		{
			m_highPass.setEnabled(false);
			m_lHighpass.setText("", NotificationType::dontSendNotification);
		}
	}
	else if (button == &m_lpfOn)
	{
		*processor.p_lpfOn = m_lpfOn.getToggleState();
		if (m_lpfOn.getToggleState() == true)
		{
			m_lowPass.setEnabled(true);
			m_lLowpass.setText(static_cast<String>(round(processor.getFreq(m_lowPass.getValue()))) + "hz", NotificationType::dontSendNotification);
		}
		else
		{
			m_lowPass.setEnabled(false);
			m_lLowpass.setText("", NotificationType::dontSendNotification);
		}
	}

	else if (button == &m_hardClip)
	{
		bool state = m_hardClip.getToggleState();
		*processor.p_hardClip = state;
		if (state)
		{
			*processor.p_softClip = false;
			m_softClip.setToggleState(false, NotificationType::dontSendNotification);
		}
	}
	else if (button == &m_softClip)
	{
		bool state = m_softClip.getToggleState();
		*processor.p_softClip = state;
		if (state)
		{
			*processor.p_hardClip = false;
			m_hardClip.setToggleState(false, NotificationType::dontSendNotification);
		}
	}
	else if (button == &aboutButton)
	{
		aboutWindow->setAlwaysOnTop(true);
		aboutWindow->toFront(true);
		aboutWindow->setVisible(true);
	}
}

void UtilityAudioProcessorEditor::sliderValueChanged(Slider * slider)
{
	if (slider == &m_panL)
	{
		*processor.p_panL = m_panL.getValue();
		getLPanText();
	}
	else if (slider == &m_panR)
	{
		*processor.p_panR = m_panR.getValue();
		getRPanText();
	}
	else if (slider == &m_prePan)
	{
		*processor.p_prePan = m_prePan.getValue();
		getPrePanText();
	}
	else if (slider == &m_gain)
	{
		*processor.p_gain = m_gain.getValue();
		getGainDB();
	}
	else if (slider == &m_pan)
	{
		*processor.p_pan = m_pan.getValue();
		getPanText();
	}
	else if (slider == &m_gainL)
	{
		*processor.p_gainL = m_gainL.getValue();
	}
	else if (slider == &m_gainR)
	{
		*processor.p_gainR = m_gainR.getValue();
	}
	else if (slider == &m_gainM)
	{
		*processor.p_gainM = m_gainM.getValue();
	}
	else if (slider == &m_gainS)
	{
		*processor.p_gainS = m_gainS.getValue();
	}
	else if (slider == &m_width)
	{
		*processor.p_width = m_width.getValue();
		getWidthText();
	}
	else if (slider == &m_highPass)
	{
		*processor.p_highPass = m_highPass.getValue();
		m_lHighpass.setText(static_cast<String>(round(processor.getFreq(m_highPass.getValue()))) + "hz", NotificationType::dontSendNotification);
	}
	else if (slider == &m_lowPass)
	{
		*processor.p_lowPass = m_lowPass.getValue();
		m_lLowpass.setText(static_cast<String>(round(processor.getFreq(m_lowPass.getValue()))) + "hz", NotificationType::dontSendNotification);
	}
}

void UtilityAudioProcessorEditor::comboBoxChanged(ComboBox * comboBoxThatHasChanged)
{
	*processor.p_input = m_input.getSelectedId();
	processor.updateHostDisplay();
	if (*processor.p_input > 1 && *processor.p_input < 5)
	{
		m_panL.setVisible(false);
		m_panR.setVisible(false);
		m_lpanL.setVisible(false);
		m_lpanR.setVisible(false);
		
		if (*processor.p_input == 2)
		{
			m_lchannel.setText("Left channel only", NotificationType::dontSendNotification);
			m_lchannel.setVisible(true);
		}
		else if (*processor.p_input == 3)
		{
			m_lchannel.setText("Right channel only", NotificationType::dontSendNotification);
			m_lchannel.setVisible(true);
		}

	}
	else
	{
		m_panL.setVisible(true);
		m_panR.setVisible(true);
		m_lpanL.setVisible(true);
		m_lpanR.setVisible(true);
		m_lchannel.setVisible(false);
	}

	if (*processor.p_input == 4)
	{
		m_prePan.setVisible(true);
		m_lprePan.setVisible(true);
		m_lchannel.setVisible(false);
	}
	else
	{
		m_prePan.setVisible(false);
		m_lprePan.setVisible(false);
	}

	repaint();
}

void UtilityAudioProcessorEditor::clipLightClicked(FFAU::LevelMeter* meter, const int channel, juce::ModifierKeys mods)
{
	if (meter == meterIn)
		meterIn->clearClipIndicator();
	else if (meter == meterOut)
		meterOut->clearClipIndicator();
}

void UtilityAudioProcessorEditor::maxLevelClicked(FFAU::LevelMeter* meter, const int channel, juce::ModifierKeys mods)
{
	if (meter == meterIn)
		meterIn->clearMaxLevelDisplay();
	else if (meter == meterOut)
		meterOut->clearMaxLevelDisplay();
}

void UtilityAudioProcessorEditor::timerCallback()
{
	if (m_panL.getValue() != *processor.p_panL)
	{
		m_panL.setValue(*processor.p_panL, NotificationType::dontSendNotification);
		getLPanText();
	}
	if (m_panR.getValue() != *processor.p_panR)
	{
		m_panR.setValue(*processor.p_panR, NotificationType::dontSendNotification);
		getRPanText();
	}
	if (m_prePan.getValue() != *processor.p_prePan)
	{
		m_prePan.setValue(*processor.p_prePan, NotificationType::dontSendNotification);
		getPrePanText();
	}
	if (m_gain.getValue() != *processor.p_gain)
		m_gain.setValue(*processor.p_gain, NotificationType::dontSendNotification);
	if (m_gainL.getValue() != *processor.p_gainL)
		m_gainL.setValue(*processor.p_gainL, NotificationType::dontSendNotification);
	if (m_gainR.getValue() != *processor.p_gainR)
		m_gainR.setValue(*processor.p_gainR, NotificationType::dontSendNotification);
	if (m_gainM.getValue() != *processor.p_gainM)
		m_gainM.setValue(*processor.p_gainM, NotificationType::dontSendNotification);
	if (m_gainS.getValue() != *processor.p_gainS)
		m_gainS.setValue(*processor.p_gainS, NotificationType::dontSendNotification);
	if (m_pan.getValue() != *processor.p_pan)
		m_pan.setValue(*processor.p_pan, NotificationType::dontSendNotification);
	if (m_width.getValue() != *processor.p_width)
		m_width.setValue(*processor.p_width, NotificationType::dontSendNotification);
	if (m_lowPass.getValue() != *processor.p_lowPass)
	{
		m_lowPass.setValue(*processor.p_lowPass, NotificationType::dontSendNotification);
		m_lLowpass.setText(static_cast<String>(std::round(processor.getFreq(m_lowPass.getValue()))) + "hz", NotificationType::dontSendNotification);
	}
	if (m_highPass.getValue() != *processor.p_highPass)
	{
		m_highPass.setValue(*processor.p_highPass, NotificationType::dontSendNotification);
		m_lHighpass.setText(static_cast<String>(round(processor.getFreq(m_highPass.getValue()))) + "hz", NotificationType::dontSendNotification);
	}

	if (m_input.getSelectedId() != *processor.p_input)
	{
		m_input.setSelectedId(*processor.p_input, NotificationType::dontSendNotification);
		if (*processor.p_input > 1 && *processor.p_input < 5)
		{
			m_panL.setVisible(false);
			m_panR.setVisible(false);
			m_lpanL.setVisible(false);
			m_lpanR.setVisible(false);

			if (*processor.p_input == 2)
			{
				m_lchannel.setText("Left channel only", NotificationType::dontSendNotification);
				m_lchannel.setVisible(true);
			}
			else if (*processor.p_input == 3)
			{
				m_lchannel.setText("Right channel only", NotificationType::dontSendNotification);
				m_lchannel.setVisible(true);
			}

		}
		else
		{
			m_panL.setVisible(true);
			m_panR.setVisible(true);
			m_lpanL.setVisible(true);
			m_lpanR.setVisible(true);
			m_lchannel.setVisible(false);
		}

		if (*processor.p_input == 4)
		{
			m_prePan.setVisible(true);
			m_lprePan.setVisible(true);
			m_lchannel.setVisible(false);
		}
		else
		{
			m_prePan.setVisible(false);
			m_lprePan.setVisible(false);
		}

		repaint();
	}

	if (m_mute.getToggleState() != *processor.p_mute)
		m_mute.setToggleState(*processor.p_mute, NotificationType::dontSendNotification);
	if (m_pad.getToggleState() != *processor.p_pad)
		m_pad.setToggleState(*processor.p_pad, NotificationType::dontSendNotification);
	if (m_DCoffset.getToggleState() != *processor.p_DCoffset)
		m_DCoffset.setToggleState(*processor.p_DCoffset, NotificationType::dontSendNotification);
	if (m_phaseL.getToggleState() != *processor.p_phaseL)
		m_phaseL.setToggleState(*processor.p_phaseL, NotificationType::dontSendNotification);
	if (m_phaseR.getToggleState() != *processor.p_phaseR)
		m_phaseR.setToggleState(*processor.p_phaseR, NotificationType::dontSendNotification);
	if (m_muteL.getToggleState() != *processor.p_muteL)
		m_muteL.setToggleState(*processor.p_muteL, NotificationType::dontSendNotification);
	if (m_muteR.getToggleState() != *processor.p_muteR)
		m_muteR.setToggleState(*processor.p_muteR, NotificationType::dontSendNotification);
	if (m_muteM.getToggleState() != *processor.p_muteM)
		m_muteM.setToggleState(*processor.p_muteM, NotificationType::dontSendNotification);
	if (m_muteS.getToggleState() != *processor.p_muteS)
		m_muteS.setToggleState(*processor.p_muteS, NotificationType::dontSendNotification);
	if (m_hpfOn.getToggleState() != *processor.p_hpfOn)
		m_hpfOn.setToggleState(*processor.p_hpfOn, NotificationType::dontSendNotification);
	if (m_lpfOn.getToggleState() != *processor.p_lpfOn)
		m_lpfOn.setToggleState(*processor.p_lpfOn, NotificationType::dontSendNotification);
	if (m_hardClip.getToggleState() != *processor.p_hardClip)
		m_hardClip.setToggleState(*processor.p_hardClip, NotificationType::dontSendNotification);
	if (m_softClip.getToggleState() != *processor.p_softClip)
		m_softClip.setToggleState(*processor.p_softClip, NotificationType::dontSendNotification);
}

void UtilityAudioProcessorEditor::getGainDB()
{
	float val = m_gain.getValue();
	String sign;
	if (val < 0.0f)
		sign = "-";
	else
		sign = " ";
	float newVal = fabs(val);
	std::string valText;
	std::stringstream stream;
	stream << std::fixed << std::setprecision(1) << newVal;
	if (newVal  < 10.0f)
		valText = "0" + stream.str();
	else
		valText = stream.str();

	m_lgainDB.setText(sign + valText + " dB", NotificationType::dontSendNotification);
}

void UtilityAudioProcessorEditor::getPanText()
{
	String pan;
	std::string valText;
	float newValue = m_pan.getValue() - 0.5;

	if (newValue < -0.00009)
		pan = " L";
	else if (newValue > 0.00009)
		pan = " R";
	else
	{
		pan = " C";
		newValue = 0.0f;
		//updateProcessor = true; //if automation value is insignificantly different from 0, change to 0
	}

	float newVal = fabs(newValue) * 100.0f;

	std::stringstream stream;
	stream << std::fixed << std::setprecision(1) << newVal;
	if (newVal < 9.999f)
		valText = "0" + stream.str();
	else
		valText = stream.str();

	m_lPan.setText(valText + pan, NotificationType::dontSendNotification);
}

void UtilityAudioProcessorEditor::getLPanText()
{
	String pan;
	std::string valText;
	float newValue = m_panL.getValue() - 0.5;

	if (newValue < -0.00009)
		pan = " L";
	else if (newValue > 0.00009)
		pan = " R";
	else
	{
		pan = " C";
		newValue = 0.0f;
		//updateProcessor = true; //if automation value is insignificantly different from 0, change to 0
	}

	float newVal = fabs(newValue) * 100.0f;

	std::stringstream stream;
	stream << std::fixed << std::setprecision(1) << newVal;
	if (newVal < 9.999f)
		valText = "0" + stream.str();
	else
		valText = stream.str();

	m_lpanL.setText(valText + pan, NotificationType::dontSendNotification);
}

void UtilityAudioProcessorEditor::getRPanText()
{
	String pan;
	std::string valText;
	float newValue = m_panR.getValue() - 0.5;

	if (newValue < -0.00009)
		pan = " L";
	else if (newValue > 0.00009)
		pan = " R";
	else
	{
		pan = " C";
		newValue = 0.0f;
		//updateProcessor = true; //if automation value is insignificantly different from 0, change to 0
	}

	float newVal = fabs(newValue) * 100.0f;

	std::stringstream stream;
	stream << std::fixed << std::setprecision(1) << newVal;
	if (newVal < 9.999f)
		valText = "0" + stream.str();
	else
		valText = stream.str();

	m_lpanR.setText(valText + pan, NotificationType::dontSendNotification);
}

void UtilityAudioProcessorEditor::getPrePanText()
{
	String pan;
	std::string valText;
	float newValue = m_prePan.getValue() - 0.5;

	if (newValue < -0.00009)
		pan = " L";
	else if (newValue > 0.00009)
		pan = " R";
	else
	{
		pan = " C";
		newValue = 0.0f;
		//updateProcessor = true; //if automation value is insignificantly different from 0, change to 0
	}

	float newVal = fabs(newValue) * 100.0f;

	std::stringstream stream;
	stream << std::fixed << std::setprecision(1) << newVal;
	if (newVal < 9.999f)
		valText = "0" + stream.str();
	else
		valText = stream.str();

	m_lprePan.setText(valText + pan, NotificationType::dontSendNotification);
}

void UtilityAudioProcessorEditor::getWidthText()
{
	float val = (m_width.getValue() + 1.0f) * 100.0f;
	std::string valText;
	std::stringstream stream;
	stream << std::fixed << std::setprecision(0) <<val;
	valText = stream.str();
	m_lWidth.setText(valText + "%", NotificationType::dontSendNotification);
}
