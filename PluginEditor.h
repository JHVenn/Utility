#ifndef PLUGINEDITOR_H_INCLUDED
#define PLUGINEDITOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "OtherLookAndFeel.h"
#include "AboutWindow.h"

//==============================================================================
/**
*/
class UtilityAudioProcessorEditor  : public AudioProcessorEditor, private ButtonListener, private SliderListener, private ComboBoxListener, private FFAU::LevelMeter::Listener, private Timer
{
public:
    UtilityAudioProcessorEditor (UtilityAudioProcessor&);
    ~UtilityAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

	void buttonClicked(juce::Button * button) override;
	void sliderValueChanged(Slider* slider) override;
	void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
	void clipLightClicked(FFAU::LevelMeter* meter, const int channel, juce::ModifierKeys mods)  override;
	void maxLevelClicked(FFAU::LevelMeter* meter, const int channel, juce::ModifierKeys mods)  override;

	Slider m_highPass;
	Slider m_lowPass;

	void timerCallback() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    UtilityAudioProcessor& processor;

	TextButton m_mute;
	TextButton m_pad;
	TextButton m_DCoffset;
	TextButton m_phaseL;
	TextButton m_phaseR;
	TextButton m_muteL;
	TextButton m_muteR;
	TextButton m_muteM;
	TextButton m_muteS;
	TextButton m_hpfOn;
	TextButton m_lpfOn;
	TextButton m_hardClip;
	TextButton m_softClip;

	Slider m_panL;
	Slider m_panR;
	Slider m_prePan;
	Slider m_gain;
	Slider m_pan;
	Slider m_gainL;
	Slider m_gainR;
	Slider m_gainM;
	Slider m_gainS;
	Slider m_width;

	ComboBox m_input;

	OtherLookAndFeel otherLookAndFeel;
	OtherLookAndFeel2 otherLookAndFeel2;
	OtherLookAndFeel3 otherLookAndFeel3;
	OtherLookAndFeel4 otherLookAndFeel4;

	ScopedPointer<FFAU::LevelMeter> meterIn;
	ScopedPointer<FFAU::LevelMeter> meterOut;
	ScopedPointer<FFAU::LevelMeterLookAndFeel> lnfIn;
	ScopedPointer<FFAU::LevelMeterLookAndFeel> lnfOut;

	Label m_lpanL;
	Label m_lpanR;
	Label m_lprePan;
	Label m_lgainDB;
	Label m_lPan;
	Label m_lWidth;
	Label m_lLowpass;
	Label m_lHighpass;
	Label m_lchannel;

	void getGainDB();
	void getPanText();
	void getLPanText();
	void getRPanText();
	void getPrePanText();
	void getWidthText();

	int horShift = 100;
	int vertShift = 65;

	Image vlogo;

	ScopedPointer<AboutWindow> aboutWindow;
	TextButton aboutButton;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (UtilityAudioProcessorEditor)
};


#endif  // PLUGINEDITOR_H_INCLUDED
